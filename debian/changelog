chemtool (1.6.14-7) UNRELEASED; urgency=medium


 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Sat, 03 Oct 2020 11:43:30 +0200

chemtool (1.6.14-6) unstable; urgency=medium

  * debian/patches/957084_gcc10.patch: New patch, fixes build failure with
    gcc10/-fno-common (Closes: #957084).

 -- Michael Banck <mbanck@debian.org>  Sat, 03 Oct 2020 11:43:13 +0200

chemtool (1.6.14-5) unstable; urgency=medium

  * d/control (Standards-Version): Bumped to 4.4.1.
  * d/rules: Enable hardening and export build flags using buildflags.mk.
  * d/p/fix-cppflags-missing.patch: Add patch.
    - Fix `dpkg-buildflags-missing CPPFLAGS 1 (of 8) missing` warning.
  * d/p/series: Add patch.

 -- Daniel Leidert <dleidert@debian.org>  Sat, 04 Jan 2020 01:53:37 +0100

chemtool (1.6.14-4) unstable; urgency=medium

  * d/compat: Raised level to 12.
  * d/control (Build-Depends): Raised debhelper version accordingly.
    (Standards-Version): Bumped to 4.4.0.
  * d/copyright: Minor update.

 -- Daniel Leidert <dleidert@debian.org>  Fri, 19 Jul 2019 17:34:22 +0200

chemtool (1.6.14-3) unstable; urgency=medium

  * Team upload.

  [ Daniel Leidert ]
  * Fix Vcs-* fields in debian/control after migrating package to Git.

  [ Andreas Tille ]
  * Standards-Version: 4.3.0
  * d/rules: Drop --parallel which is default for debhelper 11

 -- Andreas Tille <tille@debian.org>  Tue, 05 Feb 2019 15:02:36 +0100

chemtool (1.6.14-2) unstable; urgency=low

  * debian/chemtool.desktop: Removed.
  * debian/chemtool.menu: Ditto.
  * debian/chemtool_icon.xpm: Ditto.
  * debian/chemtool.install: Adjusted. Removed .menu related entries.
  * debian/compat: Raised to level 11.
  * debian/control: Sorted.
    (Build-Depends): Raised to debhelper version 11 and removed autotools-dev.
    (Standards-Version): Bumped to 4.1.3.
    (Vcs-Browser): Use secure URI.
    (Depends, Description): Replaced transfig by fig2dev (closes: #866149).
  * debian/copyright: Rewritten in format version 1.0.
  * debian/patches/748031_signedness.patch: Added.
    - Fix conflicting function definitions (closes: #748031).
  * debian/patches/866703_cross.patch: Added (thanks to Helmut Grohne).
    - Use standard autoconf macro for pkg-config detection (closes: #866703).
  * debian/patches/desktop.patch: Added.
    - Fix the .desktop file and merge contents from debian/chemtool.desktop.
  * debian/patches/series: Adjusted.
  * debian/rules: Removed obsolete autotools_dev dh add-on.
  * debian/upstream: Renamed to debian/upstream/metadata.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 16 Jan 2018 21:58:41 +0100

chemtool (1.6.14-1) unstable; urgency=low

  * New upstream release.
  * debian/control (Standards-Version): Bumped to 3.9.4.
    (Vcs-Browser, Vcs-Svn): Fixed vcs-field-not-canonical.
    (DM-Upload-Allowed): Dropped.
  * debian/rules: Enable hardening.
  * debian/upstream: Added.

 -- Daniel Leidert <dleidert@debian.org>  Tue, 13 Aug 2013 23:39:07 +0200

chemtool (1.6.13-1) unstable; urgency=low

  * New upstream release.
  * debian/control (Build-Depends): Better autotools-dev dependency.
  * debian/patches/554084_fix_ftbfs_with_no_add_needed.patch: Dropped.
    - Fixed upstream.
  * debian/patches/585912_document_special_char_limitation.patch: Dropped.
    - Unicode support added by upstream.
  * debian/patches/650112_fix_segfault_at_startup.patch: Dropped.
    - Applied upstream.
  * debian/patches/LP839745_enabling_translation.patch: Dropped.
    - Applied upstream.
  * debian/patches/series: Adjusted.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Thu, 29 Dec 2011 19:19:07 +0100

chemtool (1.6.12-2) unstable; urgency=low

  * Acknowledge NMU (closes: #554084).

  * debian/chemtool.docs: Added.
  * debian/chemtool.examples: Ditto.
  * debian/compat: Bumped to dh compatibility level 7.
  * debian/control (Uploaders): Removed LI Daobing. Thanks for your work.
    (Build-Depends): Increased required debhelper and autotools-dev versions.
    (Standards-Version): Bumped to 3.9.2.
    (Vcs-Browser): Point to the real directory.
  * debian/install: Renamed to debian/chemtool.install.
  * debian/menu: Renamed to debian/chemtool.menu.
  * debian/rules: Rewritten for dh 7. Added -Wl,--as-needed to drop unused
    dependencies.
  * debian/source/format: Added for format 3.0 (quilt).
  * debian/patches/554084_fix_ftbfs_with_no_add_needed.patch: Added.
    - Add -lX11 to chemtool_LDADD for #554084.
  * debian/patches/585912_document_special_char_limitation.patch: Added.
    - Document a limitation in special characters handling (closes: #585912).
  * debian/patches/650112_fix_segfault_at_startup.patch: Added.
    - Fix XPM color entries in bitmap1.h (closes: #650112).
  * debian/patches/LP839745_enabling_translation.patch: Added.
    - Fix gettext support initialization (LP: #839745).
  * debian/patches/series: Added.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sat, 26 Nov 2011 20:53:21 +0100

chemtool (1.6.12-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Makefile.in: Added -lX11 to chemtool_LDADD.  Fixed FTBFS.  Closes: #554084.

 -- Bart Martens <bartm@debian.org>  Sun, 02 Oct 2011 19:28:44 +0200

chemtool (1.6.12-1) unstable; urgency=low

  * New upstream release.

  * debian/chemtool.desktop: Added entry to process application/x-chemtool.
  * debian/chemtool.sharedmimeinfo: Added to make the system recognize the
    application/x-chemtool MIME type.
  * debian/control (Standards-Version): Bumped to 3.8.1.
    (Depends): Fixed debhelper-but-no-misc-depends. Added autotools-dev to fix
    outdated-autotools-helper-file.
  * debian/chemtool.sharedmimeinfo
  * debian/patches/07_manpage_fixes.patch: Dropped (applied upstream).
  * debian/patches/08_escape_characters_for_svg.patch: Ditto.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Sun, 14 Jun 2009 14:29:24 +0200

chemtool (1.6.11-2) unstable; urgency=low

  * debian/patches/08_escape_characters_for_svg.patch: Added.
    - inout.c (export_svg): Escape <, > and & in text for SVG output to not
      break it. Thanks to Martin Kroeker for the patch.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Thu, 06 Mar 2008 20:31:56 +0100

chemtool (1.6.11-1) unstable; urgency=low

  * New upstream version 1.6.11.

  [ Daniel Leidert ]
  * debian/chemtool.desktop: Removed obsolete Encoding key.
  * debian/chemtool.xpm: Renamed to debian/chemtool_icon.xpm.
  * debian/compat: Raised to 5.
  * debian/control: Vcs* fields transition.
    (Build-Depends): Raised to debhelper v5.
    (Uploaders): Added myself.
  * debian/copyright: Updated/completed.
  * debian/dirs: Removed (useless).
  * debian/docs: Dropped in favour of DEB_INSTALL_DOCS_chemtool.
  * debian/examples: Dropped in favour of DEB_INSTALL_EXAMPLES_chemtool.
  * debian/install: Removed debian/tmp/ stuff and added chemtool.xpm
    installation.
  * debian/menu (section): Fixed to comply with latest (menu) policy.
    (icon): Adjusted after icon renaming.
  * debian/rules: Added DEB_INSTALL_DOCS_chemtool and
    DEB_INSTALL_EXAMPLES_chemtool to drop docs and examples files. Removed
    useless binary-post-install target.
    (DEB_CONFIGURE_EXTRA_FLAGS): Use --without legacy option(s).
  * debian/watch: Slightly updated.
  * patches/06_suppress_opt.patch: Dropped (upstream rewrote Makefiles).
  * patches/07_manpage_fixes.patch: Added.

  [ Michael Banck ]
  * debian/control (Maintainer): Set to Debichem Team.
  * debian/control (Standards-Version): Bump to 3.7.3.
  * debian/control (Homepage): Added.
  * debian/control (DM-Upload-Allowed): Added.

 -- Daniel Leidert (dale) <daniel.leidert@wgdd.de>  Fri, 22 Feb 2008 04:56:02 +0100

chemtool (1.6.10-1) unstable; urgency=low

  * New upstream version.

  [ Michael Banck ]
  * Sync with ubuntu.
  * debian/control (Depends): Demote openbabel to Recommends.

  [ LI Daobing ]
  * debian/chemtool.desktop: fix warning for desktop-file-validate.
  * debian/watch: added.
  * change standards version from 3.6.1 to 3.7.2
  * add me to uploader

 -- LI Daobing <lidaobing@gmail.com>  Sat, 24 Mar 2007 03:04:57 +0800

chemtool (1.6.9-1ubuntu1) edgy; urgency=low

  * Merge from Debian unstable keeping previous Ubuntu changes

 -- Jordan Mantha <mantha@ubuntu.com>  Fri,  7 Jul 2006 12:35:16 -0700

chemtool (1.6.9-1) unstable; urgency=low

  * New upstream version.

 -- Michael Banck <mbanck@debian.org>  Sun, 11 Jun 2006 14:22:14 +0200

chemtool (1.6.7-2ubuntu2) dapper; urgency=low

  * Added a .desktop file from Phil Bull

 -- Emmet Hikory <emmet.hikory@gmail.com>  Sat,  8 Apr 2006 12:56:25 +0900

chemtool (1.6.7-2ubuntu1) breezy; urgency=low

  * import newer debian version; closes: #649 (malone)
  * move openbabel to depends

 -- Stefan Potyra <daemon@poleboy.de>  Sun, 18 Sep 2005 02:43:31 +0200

chemtool (1.6.7-2) unstable; urgency=low

  * debian/control (Build-Depends): Replace libgtk1.2-dev with
    libgtk2.0-dev; closes: #321511
  * debian/control (Suggests): Added fig2sxd; closes: #321510
  * debian/patches/06_suppress_opt.patch: New file, surpressing -O2
    from $CFLAGS due to run-time errors with gcc-4.0; closes: #321512

 -- Michael Banck <mbanck@debian.org>  Sat,  6 Aug 2005 23:44:34 +0200

chemtool (1.6.7-1) unstable; urgency=low

  * New upstream release.

 -- Michael Banck <mbanck@debian.org>  Wed,  3 Aug 2005 18:15:44 +0200

chemtool (1.6.6-1) unstable; urgency=low

  * New upstream release; closes: #304113
  * debian/copyright: Updated upstream location; closes: #304112

 -- Michael Banck <mbanck@debian.org>  Tue, 12 Apr 2005 02:55:54 +0200

chemtool (1.6.4-1) unstable; urgency=low

  * New upstream release.
  * (05_fix_manpage.patch): Dropped, applied upstream.

 -- Michael Banck <mbanck@debian.org>  Sat,  8 Jan 2005 02:29:22 +0100

chemtool (1.6.3-2) unstable; urgency=low

  * debian/menu (needs, section): Quote entries.
  * debian/rules.old: Removed file.
  * chemtool.1: Added section and removed to superfluous spaces.
  * debian/control (Standards-Version): Bumped to 3.6.1.0.
  * debian/chemtool.install: Renamed to...
  * debian/install: This.
  * debian/chemtool.xpm: New file, a 32x32 version of chemtool.xpm.
  * debian/install: Install debian/chemtool.xpm into /usr/share/pixmaps.

 -- Michael Banck <mbanck@debian.org>  Wed, 19 May 2004 21:47:27 +0200

chemtool (1.6.3-1) unstable; urgency=low

  * New upstream release.
  * (04_label_rendering_fix): Dropped, applied upstream.
  * (03_seperate_CFLAGS): Dropped, applied upstream.

 -- Michael Banck <mbanck@debian.org>  Tue, 18 May 2004 22:01:19 +0200

chemtool (1.6.1-2) unstable; urgency=low

  * Really include 03_seperate_CFLAGS now.
  * inout.c (exfig): Fix label rendering, patch by upstream.
    (04_label_rendering_fix)

 -- Michael Banck <mbanck@debian.org>  Tue,  4 May 2004 17:27:04 +0200

chemtool (1.6.1-1) unstable; urgency=low

  * New upstream release.
  * (02_check_fonts): Dropped, applied upstream.
  * Makefile.in (CFLAGS): Factor out CPPLAGS from CFLAGS.
    (03_seperate_CFLAGS)

 -- Michael Banck <mbanck@debian.org>  Tue,  4 May 2004 14:04:01 +0200

chemtool (1.6-3) unstable; urgency=low

  * main.c: Use GTK default font if chemtool's default is unavaiable
    (02_check_fonts); closes: #222449

 -- Michael Banck <mbanck@debian.org>  Tue, 16 Mar 2004 00:03:09 +0100

chemtool (1.6-2) unstable; urgency=low

  * Added Build-Depends on cdbs; closes: #201885
  * Bumped Standards-Version to 3.6.0

 -- Michael Banck <mbanck@debian.org>  Fri, 18 Jul 2003 15:35:46 +0200

chemtool (1.6-1) unstable; urgency=low

  * The 'Another one, and another one, and another one'-release
  * New upstream major release
  * Switched to cdbs

 -- Michael Banck <mbanck@debian.org>  Fri, 18 Jul 2003 01:16:59 +0200

chemtool (1.5+1.6alpha29-1) unstable; urgency=low

  * New upstream development release

 -- Michael Banck <mbanck@debian.org>  Sun, 20 Apr 2003 18:31:20 +0200

chemtool (1.5+1.6alpha28-1) unstable; urgency=low

  * New upstream development release
    - clean triple and quadruple bonds added; closes: #184005
  * Moved section of menu entry from Math to Science; closes: #185725
  * Include examples/* in debian/examples, not every file on its own
  * Merged gtk-upstream Hurd fix into gtkfilesel.c
  * Added -g to CFLAGS in Makefile.in
  * Removed the GTK reference from the short description

 -- Michael Banck <mbanck@debian.org>  Mon, 24 Mar 2003 16:00:08 +0100

chemtool (1.5+1.6alpha25-1) unstable; urgency=low

  * New upstream development release
  * Fixed Add_ring() to have all bonds inside the ring. Chemtool now
    handles double bonds differently

 -- Michael Banck <mbanck@debian.org>  Tue, 28 Jan 2003 18:50:09 +0100

chemtool (1.5+1.6alpha22-1) unstable; urgency=low

  * New upstream development release

 -- Michael Banck <mbanck@debian.org>  Mon, 13 Jan 2003 00:20:28 +0100

chemtool (1.5+1.6alpha21-1) unstable; urgency=low

  * New upstream development release

 -- Michael Banck <mbanck@debian.org>  Tue,  7 Jan 2003 19:56:00 +0100

chemtool (1.5+1.6alpha20-1) unstable; urgency=low

  * New upstream development release

 -- Michael Banck <mbanck@debian.org>  Thu,  2 Jan 2003 01:28:47 +0100

chemtool (1.5+1.6alpha18-1) unstable; urgency=low

  * New upstream development release

 -- Michael Banck <mbanck@debian.org>  Wed, 18 Dec 2002 01:47:32 +0100

chemtool (1.5+1.6alpha17-1) unstable; urgency=low

  * New upstream development release
  * inout.c: Fixed {fig|eps|tex}-export for italic and bold fonts
    always being Helvetica

 -- Michael Banck <mbanck@debian.org>  Thu, 28 Nov 2002 17:52:57 +0100

chemtool (1.5+1.6alpha16-1) unstable; urgency=low

  * New upstream development release

 -- Michael Banck <mbanck@debian.org>  Sun, 17 Nov 2002 20:48:10 +0100

chemtool (1.5+1.6alpha15-1) unstable; urgency=low

  * New upstream development release

 -- Michael Banck <mbanck@debian.org>  Sat,  9 Nov 2002 20:34:37 +0100

chemtool (1.5-2) unstable; urgency=low

  * New maintainer email address
  * Removed emacs-variables from debian/changelog
  * Bumped Standards-Version: to 3.5.6.1
  * Added versioned Build-Depends: for debhelper to satisfy linda

 -- Michael Banck <mbanck@debian.org>  Sat, 21 Sep 2002 17:04:42 +0200

chemtool (1.5-1) unstable; urgency=low

  * New upstream release
  * Modified debian/rules as chemtool now uses autoconf
  * Chemtool now Suggests: openbabel, which can translate between
    .pdb, .mol and other file types (excluding .cht)
  * Changed description slightly to acknowledge chemtool's new
    features
  * Added an chemtool-icon to /usr/share/pixmaps
  * Added a hint and the above icon to the menu entry

 -- Michael Banck <mbanck@gmx.net>  Thu, 11 Apr 2002 23:44:03 +0200

chemtool (1.4.1-2) unstable; urgency=low

  * debian/rules: Comments trimmed
  * debian/contro: Corrected spelling mistake; closes: #124495
  * inout.c: Fixed xbm-export; closes: #122175
  * draw.c: Fixed rounding error in Add_ring()
  * draw.c: Fixed 360 degrees rollover-bug in Put_protate()

 -- Michael Banck <mbanck@gmx.net>  Fri, 21 Dec 2001 02:04:29 +0100

chemtool (1.4.1-1) unstable; urgency=low

  * New upstream release, immeadiate bugfix for 1.4.0
  * Repackaged

 -- Michael Banck <mbanck@gmx.net>  Sat, 29 Sep 2001 11:47:07 +0000

chemtool (1.3.1-3) unstable; urgency=low

  * Backported a fallback to fixed-font if helvetica isn't
    available; closes: #110716

 -- Michael Banck <mbanck@gmx.net>  Thu,  6 Sep 2001 19:11:23 +0200

chemtool (1.3.1-2) unstable; urgency=low

  * New maintainer; closes: #100212
  * Fixed depends:, so that it actually includes ${shlibs:Depends}

 -- Michael Banck <mbanck@gmx.net>  Mon,  3 Sep 2001 20:02:06 +0200

chemtool (1.3.1-1) unstable; urgency=low

  * New upstream release.
  * Updated package to latest standards version.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Fri,  4 May 2001 17:30:13 +0200

chemtool (1.3-2) unstable; urgency=low

  * Moved section from math to science to fix override disparity.
  * Corrected typo in copyright file.
  * Removed call of deprecated dh_testversion in rules.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Mon, 26 Mar 2001 18:07:03 +0200

chemtool (1.3-1) unstable; urgency=low

  * Initial Release; closes: #88355.
  * Modified Makefile.
  * Wrote a manpage for cht, and modified manpage for chemtool.

 -- Dr. Guenter Bechly <gbechly@debian.org>  Sat,  3 Mar 2001 16:07:38 +0100
